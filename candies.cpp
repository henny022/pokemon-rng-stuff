#include <atomic>
#include <chrono>
#include <magic_enum.hpp>
#include <fmt/format.h>
#include <fmt/chrono.h>
#include <fmt/ranges.h>
#include <omp.h>
#include "rng.h"

template<typename E>
requires std::is_enum_v<E>
struct fmt::formatter<E> : fmt::formatter<std::string_view, char>
{
    auto format(E e, format_context &ctx)
    {
        if constexpr (magic_enum::detail::is_flags_v<E>)
        {
            if (auto name = magic_enum::enum_flags_name(e); !name.empty())
            {
                return this->fmt::formatter<std::string_view, char>::format(std::string_view{name.data(), name.size()},
                                                                            ctx);
            }
        }
        else
        {
            if (auto name = magic_enum::enum_name(e); !name.empty())
            {
                return this->fmt::formatter<std::string_view, char>::format(std::string_view{name.data(), name.size()},
                                                                            ctx);
            }
        }
        constexpr auto type_name = magic_enum::enum_type_name<E>();
        throw std::domain_error("Type of " + std::string{type_name.data(), type_name.size()} + " enum value: " +
                                std::to_string(magic_enum::enum_integer(e)) + " is not exists.");
    }
};

enum class Item
{
    None,
    Potion,
    Antidote,
    SuperPotion,
    GreatBall,
    Repel,
    EscapeRope,
    FullHeal,
    HyperPotion,
    UltraBall,
    MaxRepel,
    Nugget,
    Revive,
    KingsRock,
    RareCandy,
    FullRestore,
    SunStone,
    Ether,
    MoonStone,
    IronBall,
    HeartScale,
    TMFling,
    Elixir,
    MaxRevive,
    TMGrassKnot,
    PPUp,
    Leftovers,
    MaxElixir,
    TMEarthquake,
};

Item itemsHGSS[10][11] = {
        {
                Item::Potion,
                Item::Antidote,
                Item::SuperPotion,
                Item::GreatBall,
                Item::Repel,
                Item::EscapeRope,
                Item::FullHeal,
                Item::HyperPotion,
                Item::UltraBall,
                Item::MaxRepel,
                Item::Nugget,
        },
        {
                Item::Antidote,
                Item::SuperPotion,
                Item::GreatBall,
                Item::Repel,
                Item::EscapeRope,
                Item::FullHeal,
                Item::HyperPotion,
                Item::UltraBall,
                Item::Revive,
                Item::Nugget,
                Item::KingsRock,
        },
        {
                Item::SuperPotion,
                Item::GreatBall,
                Item::Repel,
                Item::EscapeRope,
                Item::FullHeal,
                Item::HyperPotion,
                Item::UltraBall,
                Item::Revive,
                Item::RareCandy,
                Item::KingsRock,
                Item::FullRestore,
        },
        {
                Item::GreatBall,
                Item::Repel,
                Item::EscapeRope,
                Item::FullHeal,
                Item::HyperPotion,
                Item::UltraBall,
                Item::Revive,
                Item::RareCandy,
                Item::SunStone,
                Item::FullRestore,
                Item::Ether,
        },
        {
                Item::Repel,
                Item::EscapeRope,
                Item::FullHeal,
                Item::HyperPotion,
                Item::UltraBall,
                Item::Revive,
                Item::RareCandy,
                Item::SunStone,
                Item::MoonStone,
                Item::Ether,
                Item::IronBall,
        },
        {
                Item::EscapeRope,
                Item::FullHeal,
                Item::HyperPotion,
                Item::UltraBall,
                Item::Revive,
                Item::RareCandy,
                Item::SunStone,
                Item::MoonStone,
                Item::HeartScale,
                Item::IronBall,
                Item::TMFling,
        },
        {
                Item::FullHeal,
                Item::HyperPotion,
                Item::UltraBall,
                Item::Revive,
                Item::RareCandy,
                Item::SunStone,
                Item::MoonStone,
                Item::HeartScale,
                Item::FullRestore,
                Item::TMFling,
                Item::Elixir,
        },
        {
                Item::HyperPotion,
                Item::UltraBall,
                Item::Revive,
                Item::RareCandy,
                Item::SunStone,
                Item::MoonStone,
                Item::HeartScale,
                Item::FullRestore,
                Item::MaxRevive,
                Item::Elixir,
                Item::TMGrassKnot,
        },
        {
                Item::UltraBall,
                Item::Revive,
                Item::RareCandy,
                Item::SunStone,
                Item::MoonStone,
                Item::HeartScale,
                Item::FullRestore,
                Item::MaxRevive,
                Item::PPUp,
                Item::TMGrassKnot,
                Item::Leftovers,
        },
        {
                Item::Revive,
                Item::RareCandy,
                Item::SunStone,
                Item::MoonStone,
                Item::HeartScale,
                Item::FullRestore,
                Item::MaxRevive,
                Item::PPUp,
                Item::MaxElixir,
                Item::Leftovers,
                Item::TMEarthquake,
        },
};

int slot_for_chance(int chance)
{
    switch (chance)
    {
        case 0 ... 29:
            return 0;
        case 30 ... 39:
            return 1;
        case 40 ... 49:
            return 2;
        case 50 ... 59:
            return 3;
        case 60 ... 69:
            return 4;
        case 70 ... 79:
            return 5;
        case 80 ... 89:
            return 6;
        case 90 ... 93:
            return 7;
        case 94 ... 97:
            return 8;
        case 98:
            return 9;
        case 99:
            return 10;
        default:
            return -1;
    }
}

int pickup(PRNG &rng)
{
    if (rng() % 10 == 0)
    {
        return slot_for_chance(rng() % 100);
    }
    else
    {
        return -1;
    }
}

using Team = std::array<int, 6>;

template<>
struct fmt::formatter<Team> : fmt::formatter<std::string_view, char>
{
    auto helper(int level, auto out)
    {
        if (level == 1)
        {
            return fmt::format_to(out, "X");
        }
        return fmt::format_to(out, "{}", level * 10 + 1);
    }

    auto format(Team team, format_context &ctx)
    {
        auto t = helper(team[0], ctx.out());
        t = fmt::format_to(t, "/");
        t = helper(team[1], t);
        t = fmt::format_to(t, "/");
        t = helper(team[2], t);
        t = fmt::format_to(t, "/");
        t = helper(team[3], t);
        t = fmt::format_to(t, "/");
        t = helper(team[4], t);
        t = fmt::format_to(t, "/");
        return helper(team[5], t);
    }
};

int check_seed(uint32_t seed, Team team)
{
    PRNG rng(seed);
    for (int i = 0; i < 14; ++i)
    {
        rng();
    }
    int candies = 0;
    for (int i = 0; i < 6; ++i)
    {
        int slot = pickup(rng);
        if (slot != -1)
        {
            auto item = itemsHGSS[team[i]][slot];
            if (item == Item::RareCandy)
            {
                candies++;
            }
        }
    }
    return candies;
}

uint32_t seed(int mdms, int hour, int delay)
{
    return mdms << 24 | hour << 16 | delay;
}

template<int offset>
std::tuple<int, Team> check_main_seed(int mdms, int hour, int delay)
{
    int min_delay = std::max(delay - offset, 0);
    int max_delay = delay + offset;
    int max_candies = 0;
    Team best_team;
    for (int p0 = 2; p0 < 4; ++p0)
    {
        for (int p1 = 2; p1 < 4; ++p1)
        {
            for (int p2 = 2; p2 < 4; ++p2)
            {
                for (int p3 = 2; p3 < 4; ++p3)
                {
                    for (int p4 = 2; p4 < 4; ++p4)
                    {
                        for (int p5 = 2; p5 < 4; ++p5)
                        {
                            Team team{p0, p1, p2, p3, p4, p5};
                            int candies = 0;
                            for (int window_delay = min_delay; window_delay <= max_delay; ++window_delay)
                            {
                                candies += check_seed(seed(mdms, hour, window_delay), team);
                            }
                            if (candies > max_candies)
                            {
                                best_team = team;
                                max_candies = candies;
                            }
                        }
                    }
                }
            }
        }
    }
    return std::make_tuple(max_candies, best_team);
}

void check_all()
{
    int max_candies = 5;
    fmt::print("running on {} threads\n", omp_get_max_threads());
    auto start = std::chrono::steady_clock::now();
#pragma omp parallel for default(none) firstprivate(max_candies)
    for (int mdms = 0; mdms <= 0xff; ++mdms)
    {
        auto tid = omp_get_thread_num();
        for (int hour = 0; hour < 24; ++hour)
        {
            auto now = std::chrono::steady_clock::now();
            //fmt::print("[{}] processing mdms {:#x} hour {}\n", tid, mdms, hour);
            for (int delay = 900; delay <= 1200; ++delay)
            {
                auto [candies, team] = check_main_seed<4>(mdms, hour, delay);
                if (candies > max_candies)
                {
                    max_candies = candies;
                    fmt::print("[{}] found better target seed {:#x}\n", tid, seed(mdms, hour, delay));
                    fmt::print("[{}] window candies: {} team: {}\n", tid, candies, team);
                }
            }
        }
    }
    auto end = std::chrono::steady_clock::now();
    fmt::print("done, took {:%H:%M:%S}\n", std::chrono::ceil<std::chrono::seconds>(end - start));
}

void print_seed(Team team, uint32_t seed)
{
    PRNG rng(seed);
    std::array<Item, 6> items;
    for (int i = 0; i < 14; ++i)
    {
        rng();
    }
    for (int i = 0; i < 6; ++i)
    {
        int slot = pickup(rng);
        if (slot == -1)
        {
            items[i] = Item::None;
        }
        else
        {
            items[i] = itemsHGSS[team[i]][slot];
        }
    }
    fmt::print("seed: {:#x} items: {}\n", seed, items);
}

void print_seed_window(Team team, uint32_t seed, int offset)
{
    for (int i = -offset; i <= offset; ++i)
    {
        print_seed(team, seed + i);
    }
}

int main(int argc, char **argv)
{
    //check_all();
    //print_seed_window({5, 7, 5, 7, 4, 9}, 0x65080417, 4);
    //print_seed_window({2, 2, 2, 2, 2, 2}, 0x3208049e, 4);
    print_seed_window({2, 2, 3, 2, 3, 2}, 0x4c090425, 4);
}
