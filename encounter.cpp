#include <array>
#include <string_view>
#include <fmt/format.h>
#include <omp.h>
#include "rng.h"

constexpr std::array<std::string_view, 25> natures{
        "Hardy",
        "Lonely",
        "Brave",
        "Adamant",
        "Naughty",
        "Bold",
        "Docile",
        "Relaxed",
        "Impish",
        "Lax",
        "Timid",
        "Hasty",
        "Serious",
        "Jolly",
        "Naive",
        "Modest",
        "Mild",
        "Quiet",
        "Bashful",
        "Rash",
        "Calm",
        "Gentle",
        "Sassy",
        "Careful",
        "Quirky",
};

/**
 * @brief Calculates the encounter slot table from the \p ranges
 *
 * @tparam size Number of entries in \p ranges
 * @tparam greater Whether to compare >= or <
 * @param ranges Table PRNG range values
 *
 * @return Encounter slot table
 */
template<size_t size, bool greater = false>
static consteval std::array<uint8_t, 100> computeTable(const std::array<int, size> &ranges)
{
    std::array<uint8_t, 100> table;

    int r = greater ? 99 : 0;
    for (size_t i = 0; i < size; i++)
    {
        uint8_t range = ranges[i];
        if constexpr (greater)
        {
            for (; r >= range; r--)
            {
                table[r] = i;
            }
        }
        else
        {
            for (; r < range; r++)
            {
                table[r] = i;
            }
        }
    }

    return table;
}

// Ground
constexpr auto grass = computeTable<12>(std::array<int, 12>{20, 40, 50, 60, 70, 80, 85, 90, 94, 98, 99, 100});
constexpr auto rocksmash = computeTable<2>(std::array<int, 2>{80, 100});
constexpr auto bug = computeTable<10, true>(std::array<int, 10>{80, 60, 50, 40, 30, 20, 15, 10, 5, 0});
constexpr auto headbutt = computeTable<6>(std::array<int, 6>{50, 65, 80, 90, 95, 100});

// Water
constexpr auto water0 = computeTable<2>(std::array<int, 2>{70, 100});
constexpr auto water1 = computeTable<3>(std::array<int, 3>{60, 80, 100});
constexpr auto water2 = computeTable<5>(std::array<int, 5>{40, 80, 95, 99, 100});
constexpr auto water3 = computeTable<5>(std::array<int, 5>{40, 70, 85, 95, 100});
constexpr auto water4 = computeTable<5>(std::array<int, 5>{60, 90, 95, 99, 100});

static const std::array encounters_gate{
        "Zubat   Lv.  5",
        "Psyduck Lv.  5",
        "Zubat   Lv.  6",
        "Psyduck Lv.  6",
        "Geodude Lv.  5",
        "Zubat   Lv.  5",
        "Psyduck Lv.  7",
        "Geodude Lv.  7",
        "Zubat   Lv.  7",
        "Zubat   Lv.  8",
        "Zubat   Lv.  7",
        "Zubat   Lv.  8",
};

static const std::array encounters{
        "Starly Lv.  2",
        "Bidoof Lv.  2",
        "Starly Lv.  3",
        "Bidoof Lv.  3",
        "Starly Lv.  3",
        "Bidoof Lv.  3",
        "Starly Lv.  4",
        "Bidoof Lv.  4",
        "Starly Lv.  4",
        "Bidoof Lv.  4",
        "Starly Lv.  4",
        "Bidoof Lv.  4",
};

bool shiny(uint32_t pid)
{
    auto psv = (pid & 0xffff) ^ ((pid >> 16) & 0xffff);
    return (psv ^ (1011 ^ 1209)) < 8;
}

template<bool detailed = false>
bool methodJ(uint32_t seed)
{
    auto found = false;
    advance_engine<PRNG> rng({seed});
    static constexpr auto movement_rate = 40; // 40 on foot, 70 on bike or in long grass
    static constexpr auto encounter_rate = 30; // 10 in water/cave, 30 in grass
    division_distribution percent_distribution(100);
    division_distribution nature_distribution(25);
    int last_encounter = 0;
    for (int steps = 1; steps < 100; ++steps)
    {
        if (percent_distribution(rng) < movement_rate)
        {
            if (steps <= last_encounter + 5)
            {
                continue;
            }
            if (percent_distribution(rng) < encounter_rate)
            {
                last_encounter = steps - 1;
                auto slot = percent_distribution(rng);
                slot = grass[slot];
                if constexpr (detailed)
                {
                    fmt::print("encountered {} (slot {}) after {} steps (advance {})\n", encounters[slot], slot, steps,
                               rng.advances - 1);
                }
                auto nature = nature_distribution(rng);
                uint32_t pid;
                do
                {
                    pid = rng() | (rng() << 16);
                } while (pid % 25 != nature);
                auto iv1 = rng();
                auto iv2 = rng();
                auto item = rng() % 100;
                if (item < 45)
                { item = 0; }
                else if (item < 95)
                { item = 1; }
                else
                { item = 2; }
                rng();
                if constexpr (detailed)
                {
                    fmt::print("gender={} pid={:08X} nature={} ability={} item={}\n",
                               (pid & 255) < 128 ? "♀" : "♂",
                               pid,
                               natures[nature],
                               pid % 2,
                               item
                    );
                    fmt::print("{} {} {} {} {} {}\n",
                               iv1 % 32, (iv1 >> 5) % 32, (iv1 >> 10) % 32,
                               (iv2 >> 5) % 32, (iv2 >> 10) % 32, iv2 % 32
                    );
                    fmt::print("occidentary or whatever={}\n", rng.advances);
                    std::puts("");
                }
                if (encounters[slot][0] == 'B'
                    && shiny(pid)
                    && (pid & 255) >= 128
//                    && ((iv1 >> 5) % 32) >= 31
//                    && (iv2 % 32) >= 26
//                    && nature == 3
                        )
                {
                    fmt::print("seed={:08X} steps={: 3} {:02} {:02} {}\n",
                               seed, steps,
                               (iv1 >> 5) % 32, iv2 % 32,
                               (pid & 255) < 128 ? "♀" : "♂");
                    found = true;
                }
            }
        }
        //fmt::print("steps={} advances={} state={:08X}\n", steps, rng.advances, rng.state);
    }
    return found;
}

auto calibrate(uint32_t seed, int target_steps)
{
    advance_engine<PRNG> rng({seed});
    static constexpr auto movement_rate = 40; // 40 on foot, 70 on bike or in long grass
    static constexpr auto encounter_rate = 10; // 10 in water/cave, 30 in grass
    division_distribution percent_distribution(100);
    division_distribution nature_distribution(25);
    int last_encounter = 0;
    for (int steps = 1; steps <= target_steps; ++steps)
    {
        if (percent_distribution(rng) < movement_rate)
        {
            if (steps <= last_encounter + 5)
            {
                continue;
            }
            if (percent_distribution(rng) < encounter_rate)
            {
                auto slot = percent_distribution(rng);
                slot = grass[slot];
                return std::make_tuple(steps == target_steps, slot);
            }
        }
    }
    return std::make_tuple(false, 0u);
}

int main()
{
    for (int offset = -100; offset < 100; ++offset)
    {
        auto [good, slot] = calibrate(0xF81002D2 + offset, 13);
        if (good)
        {
            fmt::print("{} {}\n", 700 + offset, encounters[slot]);
        }
    }
    return 0;
}

int main1()
{
    methodJ<true>(0xF81002D2);
    return 0;
}

int main2()
{
#pragma omp parallel for default(none) collapse(2)
    for (uint32_t mdms = 0; mdms < 0x100; ++mdms)
    {
        for (uint32_t hour = 0; hour < 24; ++hour)
        {
            for (uint32_t delay = 700; delay < 710; ++delay)
            {
                auto seed = (mdms << 24) | (hour << 16) | (delay + 22);
                methodJ(seed);
            }
        }
    }
    puts("done");
    return 0;
}
