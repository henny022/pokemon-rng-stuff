#include <fmt/format.h>

namespace framerate
{
constexpr auto gba = 59.7275;
constexpr auto nds_gba = 59.6555;
constexpr auto nds = 59.8261;
}

auto to_delay(auto seconds)
{
    return seconds * framerate::nds;
}

auto from_delay(auto delay)
{
    return delay / framerate::nds;
}

auto create_calibration(auto delay, auto seconds)
{
    return from_delay(delay) - seconds;
}

struct Calibration
{
    double delay;
    double seconds;

    [[nodiscard]] double value() const
    {
        return from_delay(delay) - seconds;
    }
};

int main()
{
    const double target_delay = 1894;
    double target_seconds = 16;

    const Calibration calibration{590, 14};

    double stage2 = from_delay(target_delay) - calibration.value();
    double stage1 = target_seconds + calibration.value() + .2 - from_delay(target_delay);

    while (stage1 < 14)
    {
        stage1 += 60;
    }

    fmt::print("stage1: {:.3f}\nstage2: {:.3f}\nminutes: {:.0f}\n", stage1, stage2, (stage1 + stage2) / 60);
    fmt::print("{:.0f}/{:.0f}\n", stage1 * 1000, (stage1 + stage2) * 1000);
    fmt::print("{:.0f}/{:.0f}/{:.0f}\n", stage1 * 1000, (stage1 + stage2 - 30) * 1000, (stage1 + stage2) * 1000);
}
