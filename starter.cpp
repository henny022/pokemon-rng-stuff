#include <cstdint>
#include <array>
#include <algorithm>
#include <string_view>
#include <fmt/format.h>

struct SeedGenerator
{
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;
    int delay;

    int offset = 0;

    uint32_t operator()()
    {
        uint32_t mdms = month * day + minute + second;
        uint32_t seed = (mdms << 24) + (hour << 16) + year + delay + offset;
        offset <= 0 ? offset = (-offset) + 1 : offset = -offset;
        return seed;
    }

    uint32_t initial() const
    {
        uint32_t mdms = month * day + minute + second;
        return (mdms << 24) + (hour << 16) + year + delay;
    }
};

constexpr std::array<std::string_view, 25> german_natures{
        "Robust",
        "Solo",
        "Mutig",
        "Hart",
        "Frech",
        "Kühn",
        "Sanft",
        "Locker",
        "Pfiffig",
        "Lasch",
        "Scheu",
        "Hastig",
        "Ernst",
        "Froh",
        "Naiv",
        "Mäßig",
        "Mild",
        "Ruhig",
        "Zaghaft",
        "Hitzig",
        "Still",
        "Zart",
        "Forsch",
        "Sacht",
        "Kauzig",
};

constexpr std::array<std::string_view, 25> natures{
        "Hardy",
        "Lonely",
        "Brave",
        "Adamant",
        "Naughty",
        "Bold",
        "Docile",
        "Relaxed",
        "Impish",
        "Lax",
        "Timid",
        "Hasty",
        "Serious",
        "Jolly",
        "Naive",
        "Modest",
        "Mild",
        "Quiet",
        "Bashful",
        "Rash",
        "Calm",
        "Gentle",
        "Sassy",
        "Careful",
        "Quirky",
};

template<uint32_t a, uint32_t c>
struct LCG
{
    uint32_t state;

    uint16_t operator()()
    {
        state *= a;
        state += c;
        return state / 65536;
    }
};

using PokemonRng = LCG<1103515245, 24691>;

struct IVs
{
    int hp;
    int atk;
    int def;
    int spa;
    int spd;
    int spe;
};

int main()
{
    auto nature = "Mäßig";
    auto n = std::find_if(german_natures.begin(), german_natures.end(), [&nature](auto e)
    { return e == nature; }) - german_natures.begin();
    fmt::print("{}\n", natures[n]);
    IVs min{12,0,16,0,0,18};
    IVs max{31,23,31,3,11,31};

    for (auto second: {0/**/, 1, -1, 2, -2, 3, -3/**/})
    {
        SeedGenerator generator{21, 05, 01, 3, 22, 16 + second, 1894};
        for (int i = 0; i < 200; ++i)
        {
            auto seed = generator();
            for (int advances = 0; advances <= 20; advances += 2)
            {
                PokemonRng rng{seed};
                for (int _ = 0; _ < advances; ++_)
                {
                    rng();
                }
                uint32_t pid_lo = rng();
                uint32_t pid_hi = rng();
                uint32_t pid = pid_lo | (pid_hi << 16);
                if (pid % 25 != n)
                { continue; }
                if ((pid & 0xff) < 31)
                { continue; }
#define check_iv(stat) if (stat < min.stat || stat > max.stat) continue
                {
                    auto t = rng();
                    auto hp = t & 0x1f;
                    auto atk = (t & 0x3e0) >> 5;
                    auto def = (t & 0x7c00) >> 10;
                    check_iv(hp);
                    check_iv(atk);
                    check_iv(def);
                }
                {
                    auto t = rng();
                    auto spe = t & 0x1f;
                    auto spa = (t & 0x3e0) >> 5;
                    auto spd = (t & 0x7c00) >> 10;
                    check_iv(spa);
                    check_iv(spd);
                    check_iv(spe);
                }
                fmt::print("{:08X} {} {} {}\n", seed, advances, seed - generator.initial() + generator.delay, second);
            }
        }
    }
}