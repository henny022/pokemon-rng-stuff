#include <cstdint>
#include <array>
#include <fmt/format.h>

constexpr int monthDays[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

uint8_t mdms(int month, int day, int minute, int second)
{
    return month * day + minute + second;
}

int count(const auto &array)
{
    int c = 0;
    for (const auto value: array)
    {
        if (value)
        {
            c++;
        }
    }
    return c;
}

int mdm(int second)
{
    std::array<bool, 256> mdmss{};
    for (int month = 1; month <= 12; ++month)
    {
        for (int day = 1; day < monthDays[month]; ++day)
        {
            for (int minute = 1; minute < 60; ++minute)
            {
                mdmss[mdms(month, day, minute, second)] = true;
            }
        }
    }
    return count(mdmss);
}

int main()
{
    for (int second = 0; second < 60; ++second)
    {
        if (mdm(second) != 256)
        {
            fmt::print("bad second: {}\n", second);
        }
    }
}
