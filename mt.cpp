#include <cstdint>
#include <random>
#include <fmt/format.h>

uint32_t sMTRNG_State[624];
int sMTRNG_Cycles = 625;
uint32_t sMTRNG_XOR[2] = {0, 0x9908B0DF};


void SetMTRNGSeed(uint32_t seed)
{
    sMTRNG_State[0] = seed;
    for (sMTRNG_Cycles = 1; sMTRNG_Cycles < 624; sMTRNG_Cycles++)
    {
        sMTRNG_State[sMTRNG_Cycles] =
                1812433253 * (sMTRNG_State[sMTRNG_Cycles - 1] ^ (sMTRNG_State[sMTRNG_Cycles - 1] >> 30)) +
                sMTRNG_Cycles;
    }
}

uint32_t MTRandom()
{
    uint32_t val;
    int32_t i;

    if (sMTRNG_Cycles >= 624)
    {
        if (sMTRNG_Cycles == 625)
        {
            SetMTRNGSeed(5489);
        }

        for (i = 0; i < 227; i++)
        {
            val = (sMTRNG_State[i] & 0x80000000) | (sMTRNG_State[i + 1] & 0x7fffffff);
            sMTRNG_State[i] = sMTRNG_State[i + 397] ^ (val >> 1) ^ sMTRNG_XOR[val & 0x1];
        }
        for (; i < 623; i++)
        {
            val = (sMTRNG_State[i] & 0x80000000) | (sMTRNG_State[i + 1] & 0x7fffffff);
            sMTRNG_State[i] = sMTRNG_State[i + -227] ^ (val >> 1) ^ sMTRNG_XOR[val & 0x1];
        }

        val = (sMTRNG_State[623] & 0x80000000) | (sMTRNG_State[0] & 0x7fffffff);
        sMTRNG_State[623] = sMTRNG_State[396] ^ (val >> 1) ^ sMTRNG_XOR[val & 0x1];

        sMTRNG_Cycles = 0;
    }

    val = sMTRNG_State[sMTRNG_Cycles++]; // has to be this way in order to match

    val ^= val >> 11;
    val ^= (val << 7) & 0x9d2c5680;
    val ^= (val << 15) & 0xefc60000;
    val ^= val >> 18;

    return val;
}

bool test(uint32_t seed)
{
    SetMTRNGSeed(seed);
    std::mt19937 mt(seed);
    for (int i = 0; i < 1000000000; ++i)
    {
        if (i < 23020)
        {
            continue;
        }
        if (MTRandom() != mt())
        {
            return false;
        }
    }
    return true;
}

int main()
{
    std::mt19937 seeds;
    for (int i = 0; i < 1000000000; ++i)
    {
        auto seed = seeds();
        fmt::print("seed 0x{:08x} ({: 10}/{})\n", seed, i, 1000000000);
        if (!test(seed))
        {
            fmt::print("bad i {}\n", seed);
            return 1;
        }
    }
    return 0;
}
