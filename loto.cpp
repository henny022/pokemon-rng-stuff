#include <fmt/format.h>
#include "rng.h"

int loto_digits(MTRNG mt, uint16_t tid)
{
    ARNG group_rng(mt());
    group_rng();// current day, useless
    auto loto = MRNG(group_rng())();
    int digits = 0;
    for (; digits < 5; digits++)
    {
        if (loto % 10 != tid % 10)
        {
            break;
        }
        loto /= 10;
        tid /= 10;
    }
    return digits;
}

uint8_t mdms(int month, int day, int minute, int second)
{
    return month * day + minute + second;
}

template<int MONTH, int DAY, int MINUTE, int SECOND>
struct MDMS
{
    static constexpr int monthDays[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    int month = MONTH == -1 ? 1 : MONTH;
    int day = DAY == -1 ? 1 : DAY;
    int minute = MINUTE == -1 ? 0 : MINUTE;
    int second = SECOND == -1 ? 0 : SECOND;

    int operator()()
    {
        if (month == 13)
        {
            return -1;
        }
        int r = month * day + minute + second;
        if (SECOND != -1)
        {
            second++;
            if (second < 60)
            {
                return r;
            }
        }
        if (MINUTE != -1)
        {
            minute++;
            if (minute < 60)
            {
                return r;
            }
        }
        if (DAY != -1)
        {
            day++;
            if (day <= monthDays[month])
            {
                return r;
            }
        }
        if (MONTH != -1)
        {
            month++;
        }
        return r;
    }
};

int main()
{
    loto_digits(MTRNG(0x391702E2), 1011);
    MDMS<4, -1, 50, -1> gen;
    int mdms;
    while ((mdms = gen()) != -1)
    {
        for (int delay = 600; delay < 10000; ++delay)
        {
            uint32_t seed = (mdms << 24) | (23 << 16) | (delay + 22);
            MTRNG mt(seed);
            auto digits = loto_digits(mt, 1011);
            if (digits == 3)
            {
                fmt::print("{:08X} {}\n", seed, delay);
                break;
            }
        }
    }
}
