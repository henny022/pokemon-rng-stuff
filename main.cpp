#include <fmt/format.h>

#include <cstdint>
#include <tuple>
#include <array>
#include <random>
#include <string_view>

constexpr std::array<std::string_view, 25> natures{
        "Hardy",
        "Lonely",
        "Brave",
        "Adamant",
        "Naughty",
        "Bold",
        "Docile",
        "Relaxed",
        "Impish",
        "Lax",
        "Timid",
        "Hasty",
        "Serious",
        "Jolly",
        "Naive",
        "Modest",
        "Mild",
        "Quiet",
        "Bashful",
        "Rash",
        "Calm",
        "Gentle",
        "Sassy",
        "Careful",
        "Quirky",
};

struct RTC
{
    static constexpr int monthDays[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;
};

uint32_t seed(const RTC &rtc, int delay)
{
    return rtc.year + rtc.month * 0x100 * rtc.day * 0x10000 +
           rtc.hour * 0x10000 + (rtc.minute + rtc.second) * 0x1000000 + delay;
}

uint16_t LCRandom(uint32_t &state)
{
    state *= 1103515245;
    state += 24691;
    return state / 65536;
}

void find(uint32_t seed, RTC rtc)
{
    auto Mdms = seed >> 24;
    auto t = (seed >> 16) & 0xff;
    auto hi_delay = t - rtc.hour;
    auto lo_delay = (seed & 0xffff) + (2000 - rtc.year);  // why + not -???
    auto delay = (hi_delay << 16) + lo_delay;
    // for (int month = 1; month <= 12; month++) {
    for (int day = 1; day <= 31; day++)
    {
        for (int minute = 0; minute < 60; minute++)
        {
            // for (int second = 0; second < 60; second++) {
            if (((rtc.month * day + minute + rtc.second) & 0xff) == Mdms)
            {
                fmt::print("{: 2}.{}.{} {: 2}:{:02}:{:02} delay: {}\n", day,
                           rtc.month, rtc.year, rtc.hour, minute, rtc.second,
                           delay);
                return;
                // return std::make_tuple(delay);
            }
            //}
        }
    }
    //}
}

struct IVs
{
    char hp;
    char atk;
    char def;
    char spd;
    char sp_atk;
    char sp_def;
};

auto poke(int wanted_ability, IVs ivs)
{
    for (uint64_t _seed = 0; _seed < 0x80000000; _seed++)
    {
        uint32_t seed = _seed;
        uint32_t pid_lo = LCRandom(seed);
        uint32_t pid_hi = LCRandom(seed);
        uint32_t pid = pid_lo | (pid_hi << 16);

        if (128 > (uint8_t) pid)
        { continue; }
        auto ability = pid & 1;
        if (ability != wanted_ability)
        { continue; }
        auto nature = pid % 25;
        if (nature != 15)
        { continue; }

        auto t = LCRandom(seed);
        auto hp = t & 0x1f;
        if (hp != ivs.hp)
        { continue; }
        auto atk = (t & 0x3e0) >> 5;
        if (atk != ivs.atk)
        { continue; }
        auto def = (t & 0x7c00) >> 10;
        if (def != ivs.def)
        { continue; }

        auto t2 = LCRandom(seed);
        auto spd = t2 & 0x1f;
        if (spd != ivs.spd)
        { continue; }
        auto spatk = (t2 & 0x3e0) >> 5;
        if (spatk != ivs.sp_atk)
        { continue; }
        auto spdef = (t2 & 0x7c00) >> 10;
        if (spdef != ivs.sp_def)
        { continue; }

        fmt::print("seed: 0x{:08x} pid: 0x{:08x} ability: {}\n", seed, pid, ability);
    }
}

void egg(auto initial_seed)
{
    std::mt19937 mt(initial_seed);
    for (int advance = 0; advance < 1000; ++advance)
    {
        auto pid = mt();
        if (pid == 0x88888888)
        {
            fmt::print("seed: 0x{:08x} pid: 0x{:08x} advance: {}\n", initial_seed, pid, advance);
        }
    }
}

void for_all_seeds(void(*f)(uint32_t))
{
    for (uint64_t i = 0; i < 0x100000000; ++i)
    {
        uint32_t initial_seed = i;
        f(initial_seed);
    }
}

void for_initial_seeds(void(*f)(uint32_t))
{
    for (int mdms = 0; mdms < 256; ++mdms)
    {
        for (int hour = 0; hour < 24; ++hour)
        {
            for (int delay = 600; delay < 10000; ++delay)
            {
                uint32_t initial_seed = (mdms << 24) + (hour << 16) + delay;
                f(initial_seed);
            }
        }
    }
}

struct PID
{
    uint16_t lo;
    uint16_t hi;

    explicit PID(uint32_t pid) : lo(pid & 0xffff), hi((pid & 0xffff0000) >> 16)
    {}

    [[nodiscard]] uint32_t value() const
    {
        return (hi << 16) | lo;
    }

    [[nodiscard]] uint16_t psv() const
    {
        return lo ^ hi;
    }

    [[nodiscard]] uint32_t nature() const
    {
        return value() % 25;
    }
};

int main()
{
    // auto [delay] = find(0x12345678, 2022, 16);
    // fmt::print("delay: {}\n", delay);
    //poke(1, {31, 31, 31, 31, 31, 31});
    for_initial_seeds(egg);
    return 0;

    PID pid(0xF6CEF180);
    fmt::print("nature: {}\n", natures[pid.nature()]);
    uint16_t tsv = pid.psv() & 0xfff8;
    if ((pid.psv() ^ tsv) < 8)
    {
        fmt::print("shiny {} {}\n", (16844 ^ 18049) / 8, pid.psv() / 8);
    }
    else
    {
        fmt::print("not shiny\n");
    }
}
