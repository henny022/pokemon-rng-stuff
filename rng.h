#ifndef POKEMON_TEST_RNG_H
#define POKEMON_TEST_RNG_H

#include <cstdint>
#include <random>
#include <concepts>

template<uint32_t multiplier, uint32_t increment, uint32_t divide = 1>
struct LCG
{
    uint32_t state;

    uint32_t operator()()
    {
        state *= multiplier;
        state += increment;
        return state / divide;
    }
};

template<std::invocable generator>
struct advance_engine : generator
{
    size_t advances = 0;

    uint32_t operator()()
    {
        advances++;
        return generator::operator()();
    }
};

struct modulo_distribution
{
    uint32_t modulo;

    template<std::invocable RNG>
    uint32_t operator()(RNG rng)
    {
        return rng() % modulo;
    }
};

struct division_distribution
{
    uint32_t divisor;

    template<std::invocable RNG>
    uint32_t operator()(RNG &rng)
    {
        uint16_t rand = rng();
        uint16_t t = ((0xffff / divisor) + 1);
        return rand / t;
    }
};

using PRNG = LCG<1103515245, 24691, 65536>;
using ARNG = LCG<0x6C078965, 1>;
using MRNG = LCG<0x41c64e6d, 0x3039, 65536>;
using MTRNG = std::mt19937;

#endif //POKEMON_TEST_RNG_H
