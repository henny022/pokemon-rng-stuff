cmake_minimum_required(VERSION 3.14)
cmake_policy(SET CMP0135 NEW)
project(pokemon_test)

set(CMAKE_CXX_STANDARD 23)

include(FetchContent)

FetchContent_Declare(
        fmt
        URL https://github.com/fmtlib/fmt/archive/refs/tags/8.1.1.zip
)
FetchContent_Declare(
        magic_enum
        URL https://github.com/Neargye/magic_enum/archive/refs/tags/v0.8.2.zip
)
FetchContent_MakeAvailable(fmt magic_enum)

find_package(OpenMP REQUIRED)

add_library(project_settings INTERFACE)
target_compile_options(project_settings INTERFACE -Werror)

add_executable(pokemon_test main.cpp)
target_link_libraries(pokemon_test project_settings fmt::fmt)

add_executable(mdms mdms.cpp)
target_link_libraries(mdms project_settings fmt::fmt)

add_executable(mt mt.cpp)
target_link_libraries(mt project_settings fmt::fmt)

add_executable(timer timer.cpp)
target_link_libraries(timer project_settings fmt::fmt)

add_executable(starter starter.cpp)
target_link_libraries(starter project_settings fmt::fmt)

add_executable(candies candies.cpp)
target_link_libraries(candies project_settings fmt::fmt magic_enum::magic_enum OpenMP::OpenMP_CXX)

add_executable(loto loto.cpp)
target_link_libraries(loto project_settings fmt::fmt)

add_executable(encounter encounter.cpp)
target_link_libraries(encounter project_settings fmt::fmt OpenMP::OpenMP_CXX)
